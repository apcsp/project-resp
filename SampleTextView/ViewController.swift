import UIKit
import Foundation


class ViewController: UIViewController,UITextViewDelegate  {
    
    /* Outlets */
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var changeFontButton: UIButton!
    
    /* Class Values */
    let fontOne = UIFont(name: "Optima", size: 20.0)!
    let fontTwo = UIFont(name: "Optima", size: 30)!
    let startingTag = "h1"
    let endingTag = "h1end"

    
    /* Load View */
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.delegate = self

    }
    func textViewDidChange(_ textView: UITextView) {
        
        let testingText = textView.text
        if testingText?.range(of:"h1") != nil && testingText?.range(of:"h1end") != nil {
            textView.attributedText = modifyTextFontBetweenTags(startTag: startingTag, endTag: endingTag, text: testingText!, tagFont: fontTwo, contentsFont: fontOne)
        }
     
    }

    /* Button Action */
    @IBAction func changeFontAction(_ sender: UIButton) {
            let testingText = textView.text
        textView.attributedText = modifyTextFontBetweenTags(startTag: startingTag, endTag: endingTag, text: testingText!, tagFont: fontTwo, contentsFont: fontOne)
    }
    
    /* Tag Parsing Function */
    func modifyTextFontBetweenTags(startTag: String, endTag: String, text: String, tagFont: UIFont?, contentsFont: UIFont?) -> NSMutableAttributedString? {
        if !text.contains(startTag) || !text.contains(endTag) { return nil } // Making sure tags are actually in the text.
        guard let tagFont = tagFont, let contentsFont = contentsFont else { return nil } // Making sure our fonts are valid & tags are in the right place.
        
        let endTagSplitArray = text.components(separatedBy: endTag) // Split text by endTags.
        
        var allTagsSplitArray = [String]() // Create empty array to hold all text components after splitting.
        for item in endTagSplitArray {
            if item.contains(startTag) {
                allTagsSplitArray.append(contentsOf: item.components(separatedBy: startTag)) // Separate this text by startTag & append.
            } else {
                allTagsSplitArray.append(item) // Just append this text.
            }
        }
        
        let finalAttrStr = NSMutableAttributedString() // Create empty Mutable Attributed String
        for i in stride(from: 0, to: allTagsSplitArray.count, by: 1) {
            finalAttrStr.append(NSMutableAttributedString(string: allTagsSplitArray[i], attributes: [NSFontAttributeName : i % 2 == 0 ? tagFont : contentsFont])) // If the text was between tags, modify its font.
        }
        
        return finalAttrStr // Return completely modified text.
    }
}
